cmake_minimum_required(VERSION 3.16)
project(homework_IV)

set(CMAKE_CXX_STANDARD 20)

add_executable(homework_IV main.cpp Point.cpp Point.h CHull.cpp CHull.h)