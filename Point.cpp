#include "Point.h"

Point::Point(int id, int x, int y) : id(id), x(x), y(y) {}

Point::Point() {
    id = 0;
    x = y = 0;
}

Point::~Point() {}

int Point::getX() const {
    return x;
}

int Point::getY() const {
    return y;
}

int Point::getId() const {
    return id;
}

bool Point::operator==(const Point &rhs) const {
    return x == rhs.x &&
           y == rhs.y &&
           id == rhs.id;
}

bool Point::operator!=(const Point &rhs) const {
    return !(rhs == *this);
}

Point::Point(const Point &p2) {
    id = p2.getId();
    x = p2.getX();
    y = p2.getY();
}

