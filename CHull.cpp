#include "CHull.h"
#include <string>
#include <iostream>
#include <tuple>
#include <functional>

int CHull::p0i;
Point CHull::p0;

CHull::CHull(const std::vector<Point> &points) : points(points) {}


bool CHull::sort_angle(Point p1, Point p2) {
    return CHull::less_than(p1, p2, CHull::p0);
}

bool CHull::less_than(Point p1, Point p2, Point p3) {
    int val = (p2.getY() - p1.getY()) * (p3.getX() - p2.getX()) -
              (p2.getX() - p1.getX()) * (p3.getY() - p2.getY());

    return val <= 0;
}

std::tuple<Point, int> CHull::getPointMinY() {
    Point point = points[0];
    int index = 0;

    for (auto i = 1; i < points.size(); i++) {
        if (points[i].getY() < point.getY()
            || (points[i].getY() == point.getY() && points[i].getX() < point.getX())) {
            point = points[i];
            index = i;
        }
    }

    return std::make_tuple(point, index);
}

void CHull::solveGrahamScan() {
    if (points.size() < 3) {
        std::cout << "At least 3 points are needed!\n";
        return;
    }

    auto tuple = getPointMinY();
    CHull::p0 = std::get<0>(tuple);
    CHull::p0i = std::get<1>(tuple);

    setFirstPoint();

    std::sort(points.begin() + 1, points.end(), sort_angle);

    edge_points.push(points[0]);
    edge_points.push(points[1]);
    edge_points.push(points[2]);

    for (int i = 3; i < points.size(); i++) {
        Point t2 = getSecondTopInStack();
        Point t1 = edge_points.top();

        while (not less_than(t2, t1, points[i])) {
            edge_points.pop();

            t2 = getSecondTopInStack();
            t1 = edge_points.top();
        }

        edge_points.push(points[i]);
    }

}

std::ostream &operator<<(std::ostream &os, const CHull &hull) {
    std::string ret = "";
    std::stack<Point> points = hull.edge_points;

    while (points.size()) {
        Point p = points.top();
        ret = "P" + std::to_string(p.getId()) + ", " + ret;
        points.pop();
    }

    os << ret.substr(0, ret.length() - 2);

    return os;
}

Point CHull::getSecondTopInStack() {
    Point t1 = edge_points.top();
    edge_points.pop();
    Point t2 = edge_points.top();
    edge_points.push(t1);

    return t2;
}

void CHull::setFirstPoint() {
    Point tmp = points[CHull::p0i];
    points[CHull::p0i] = points[0];
    points[0] = tmp;
}
