#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include "CHull.h"
#include "Point.h"

using std::cout;

std::vector<Point> getPointsFromFile(std::string file) {
    std::vector<Point> points;
    std::string line;
    std::ifstream input_file(file);
    unsigned int lines = 0;

    if (input_file.is_open()) {
        while (getline(input_file, line)) {
            auto commaPos = line.find(",");

            if (commaPos != std::string::npos) {
                int x = atoi(line.substr(0, commaPos).c_str());
                int y = atoi(line.substr(commaPos + 1, line.size()).c_str());

                points.push_back(Point(lines, x, y));
                lines++;
            }
        }
        input_file.close();
    } else {
        std::cout << "Unable to open file \"" << file << "\".\n";
        exit(4);
    }

    return points;
}

int main() {
    std::vector<Point> points = getPointsFromFile("../input");

    CHull ch(points);
    ch.solveGrahamScan();

    cout << "Edge points: " << ch << '\n';

    return 0;
}
