#ifndef HOMEWORK_IV_CHULL_H
#define HOMEWORK_IV_CHULL_H


#include "Point.h"
#include <vector>
#include <algorithm>
#include <ostream>
#include <stack>

class CHull {
private:
    std::vector<Point> points;
    std::stack<Point> edge_points;

    static bool sort_angle(Point p1, Point p2);
    static bool less_than(Point p1, Point p2, Point p3);
    std::tuple<Point, int> getPointMinY();
    Point getSecondTopInStack();

public:
    static int p0i;
    static Point p0;

    CHull(const std::vector<Point> &vector);

    void solveGrahamScan();

    friend std::ostream &operator<<(std::ostream &os, const CHull &hull);

    void setFirstPoint();
};

#endif //HOMEWORK_IV_CHULL_H
