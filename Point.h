#ifndef HOMEWORK_IV_POINT_H
#define HOMEWORK_IV_POINT_H

#include <string>

class Point {
private:
    int x;
    int y;
    int id;
public:
    Point(int id, int x, int y);

    Point(const Point &p2);

    int getId() const;

    Point();

    bool operator==(const Point &rhs) const;

    bool operator!=(const Point &rhs) const;

    virtual ~Point();

    int getX() const;

    int getY() const;

};


#endif //HOMEWORK_IV_POINT_H
